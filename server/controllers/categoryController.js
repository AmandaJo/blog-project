
const Category = require('../models/categoryModels.js')


const create = async (req,res) => {
	const { category } = req.body
	console.log('category =>',category)
	try {
		const response = await Category.create({category:category})
		res.send(response)
	}	catch( err ){
	       res.send(err)   
	}
}

const remove = async (req,res) => {
	const { _id } = req.body
	console.log(req.body)
	try{
const response = await Category.deleteOne({_id:_id}) 
		res.send(response)
}		catch( err ){
		res.send(err)
           
}
}

const update = async (req, res) =>{
	const { category , _id} = req.body
	console.log(req.body)
	try{
const response = await Category.updateOne({_id:_id},{category:category}) 
res.send(response)
}	catch( err ){
	res.send(err)
           
}
}

const get_all = async (req, res) =>{
	console.log('================>')
	try{
const response = await Category.find({}) 
	res.send(response)
}      catch( err ){
		res.send(err)
           
	}
}

const get_one = async (req, res) =>{
	const { _id } = req.params 
	try{
		const response = await Category.findOne({_id:_id}) 
       res.send(response)
	}catch( err ){
	res.send(err)
           
	}
}




module.exports = {
	create,
	remove,
	update,
	get_all,
	get_one
	
}