const Posts = require('../models/postsModels.js')


const create = async (req,res) => {
	const { title, category, description, creator, images } = req.body
		console.log(title, category, description, creator, 'see if there is an ammray',images)
	if( !title || !category || !description || !creator ) return res.json({ok:false,message:'All field are required'});
	try {
		const response = await Posts.create({posts:title,
		                                     category:category, 
		                                     description:description, 
		                                     creator:creator,
		                                     images:images
		                                      })
		res.json({ok:true,message:'Post successfully created',response:response})
	}	catch( err ){
	       res.json(err) // res.json === res.send  
	}
}

const remove = async (req,res) => {
	const { _id, posts } = req.body
	console.log(req.body)
	try{
const response = await Posts.deleteOne({_id:_id,
                                           posts:posts  
                                            }) 
		res.json(response)
	}	catch( err ){
		res.json(err)
           
	}
}
const update = async (req, res) =>{
	const { posts, category, _id, description, creator, images} = req.body
	console.log(req.body)
	try{
const response = await Posts.updateOne({posts:posts,
		                                     category:category, 
		                                     description:description, 
		                                     creator:creator,
		                                     images: [{url:'https://img.freepik.com/free-photo/tulips-bouquet-pink-background-with-copyspace_24972-271.jpg?size=626&ext=jpg'}]
		                                      }) 
res.json(response)
}	catch( err ){
	res.json(err)
           
}
}

const get_all = async (req, res) =>{
	console.log('=====>')
	try{
		const response = await Posts.find({}) 
       res.json(response)
	}
	catch( err ){
		res.json(err)
           
	}
}

const get_one = async (req, res) =>{
	const { _id } = req.params 
	console.log(_id)
	try{
		const response = await Posts.findOne({_id:_id}) 
        res.json(response)
	}catch( err ){
	res.json(err)
           
	}
}



module.exports = {
	create,
	remove,
	update,
	get_all,
	get_one

	
	}