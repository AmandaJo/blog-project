const Comment = require('../models/commentModels.js')




const create = async (req,res) => {
	const { comment, posts_id, createdAt, creator } = req.body
	console.log(req.body)
	try {
		var date = new Date().toDateString()
		const response = await Comment.create({comment:comment,
		                                       posts_id:posts_id,
		                                       createdAt:createdAt,
		                                       creator:creator
		                                      })
		res.send(response)
	}	catch( err ){
	       res.send(err)   
	}
}

const remove = async (req,res) => {
	const { _id, comment } = req.body
	console.log(req.body)
	try{
const response = await Comment.deleteOne({_id:_id,
                                           comment:comment 
                                            }) 
		res.send(response)
	}	catch( err ){
		res.send(err)
           
	}
}

const update = async (req, res) =>{
	const { comment, posts_id} = req.body
	console.log(req.body)
	try{
const response = await Comment.updateOne({comment:comment,
		                                posts_id:posts_id,
		                                }) 
res.send(response)
}	catch( err ){
	res.send(err)
           
}
}

const get_by_id = async (req, res) =>{
	const { _id } = req.params
	
	try{
		const response = await Comment.find({posts_id:_id}) 
		console.log(response)
        res.send(response)
	}
	catch( err ){
		res.send(err)
           
	}
}

const get_one = async (req, res) =>{
	const {_id } = req.params 
	try{
		const response = await Comment.findOne({_id:_id}) 
       res.send(response)
	}
	catch( err ){
		res.send(err)
           
	}
}




module.exports = {
	create,
	remove,
	update,
	get_by_id,
	get_one
}