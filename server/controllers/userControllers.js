const User       = require('../models/userModels'); 
const bcrypt     = require('bcrypt');
const jwt        = require('jsonwebtoken');
const config     = require('../config.js');
const saltRounds = 10;


const register = async (req,res) => {
  const { email , username , password , password2, admin_secret } = req.body;
   console.log(req.body)
	if( !email || !password || !password2 || !username ) return res.json({ok:false,message:'All field are required'});
    if(  password !== password2 ) return res.json({ok:false,message:'passwords must match'});

     if(admin_secret && admin_secret !== config.admin_secret) return res.json({ok:false,message : 'wrong secret word'})
    try{
    	const user = await User.findOne({ email })
    	if( user ) return res.json({ok:false,message:'email already in use'});
    	const hash = await bcrypt.hash(password, saltRounds)
        console.log('hash =' , hash)
        const newUser = {
        	email,
        	password : hash,
          admin: admin_secret ? true : false,
          username: username
          //admin:true
        }
        const create = await User.create(newUser)
        res.json({ok:true,message:'successful register'})
    }catch( error ){
        res.json({ok:false,error})
    }
}

const login = async (req,res) => {
    const { email , password } = req.body;
	if( !email || !password ) res.json({ok:false,message:'All fields are required'});
	try{
    	const user = await User.findOne({ email });
    	if( !user ) return res.json({ok:false,message:'plase provide a valid email'});
        const match = await bcrypt.compare(password, user.password);
        if(match) {
           const token = jwt.sign(user.toJSON(), config.secret ,{ expiresIn:100080 });
           res.json({ok:true,message:'welcome back',token,email}) 
        }else return res.json({ok:false,message:'invalid password'})
        
    }catch( error ){
    	 res.json({ok:false,error})
    }
}

const verify_token = (req,res) => {
  console.log(req.body.token)
       const { token } = req.body;
       const decoded   = jwt.verify(token, config.secret, (err,succ) => {
             err ? res.json({ok:false,message:'something went wrong'}) : res.json({ok:true,message:'user found',decoded:succ})
       });      
}



module.exports = { register , login , verify_token }