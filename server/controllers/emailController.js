const nodemailer = require('nodemailer')
const config     = require('../config.js');
// selecting mail service and authorazing with our credentials
const transport = nodemailer.createTransport({
// you need to enable the less secure option on your gmail account
// https://myaccount.google.com/lesssecureapps?pli=1
	service: 'Gmail',
	auth: {
		user: config.user,
		pass: config.pass            
	}
});

const send_contact_email = async (req,res) => {
	  const { email, firstname, lastname, subject, message } = req.body 
	  const default_subject = 'This is a default subject'
	  const mailOptions = {
		    to: "web.amandajohnsen@gmail.com",
		    subject: "New message from " + firstname,
		    html: '<p>'+(subject || default_subject)+ '</p><p><pre>' + message + '</pre></p>'
	   }
      try{
           const response = await transport.sendMail(mailOptions)
           console.log('=========================================> Email sent !!')
           return res.json({on:true,message:'email sent'})
      }
      catch( err ){
           return res.json({ok:false,message:err})
      }
}

module.exports = { send_contact_email }