const router = require('express').Router()
const controller = require('../controllers/commentController.js')

router.post('/create',     controller.create)
router.post('/remove',     controller.remove)
router.post('/update',     controller.update)
router.get('/get_by_id/:_id',   controller.get_by_id)
router.get('/get_one/:_id',controller.get_one)


module.exports = router 