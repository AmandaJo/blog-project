const router = require('express').Router()
const controller = require('../controllers/categoryController.js')

router.post('/create',     controller.create)
router.post('/remove',     controller.remove)
router.post('/update',     controller.update)
router.get('/get_all',     controller.get_all)
router.get('/get_one/:_id',controller.get_one)


module.exports = router 