const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    email:{ type:String, unique:true, required:true },
    username:{ type:String, unique:true, required:true },
    password:{ type:String, required:true },
    admin: {type:Boolean,required:true,default:false}
});
module.exports = mongoose.model('users', userSchema);