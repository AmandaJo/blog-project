const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const postsSchema = new Schema({
    posts:{
          type:String, 
    	  required:true,
    	},
   category:{
          type:String, 
    	  required:true,
    	},
    description:{
          type:String, 
    	  required:true,
    	},
    	creator:{
          type:String, 
    	  required:true,
    	},
      images:{
        type: Array,
      } 

})
module.exports =  mongoose.model('posts', postsSchema);