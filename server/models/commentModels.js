const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const commentSchema = new Schema({
    comment:{
        type:String, 
    	  required:true
    	},
   createdAt:{
         type: String,  
         required:true 
    	},
    posts_id:{
          type:String, 
    	    required:true
    	},
   creator:{
          type:String 
   }

})
module.exports =  mongoose.model('comment', commentSchema);