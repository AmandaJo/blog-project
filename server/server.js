const path = require(`path`);

    const express = require('express'),
    app = express(),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    cors = require ('cors'),
    port = process.env.PORT || 5001,
    config = require('./config.js');

// =================== initial settings ===================
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
// connnect to mongo
app.use(cors())
async function connecting(){
try {
    await mongoose.connect(`mongodb+srv://amandajohnsen2:${config.mlab_pass}@cluster0-kcc0h.mongodb.net/blog-project?retryWrites=true&w=majority`, {  useNewUrlParser: true })
    console.log('Connected to the DB')
} catch ( error ) {
    console.log('ERROR: Seems like your DB is not running, please start it up !!!');
}
}

connecting()
// temp stuff to suppress internal warning of mongoose which would be updated by them soon
mongoose.set('useCreateIndex', true);
// end of connecting to mongo and checking if DB is running

// routes
app.use('/category', require('./routes/categoryRoutes.js'));
app.use('/posts', require('./routes/postsRoutes.js'));
app.use('/comment', require('./routes/commentRoutes.js'));
app.use('/users', require('./routes/usersRoute.js'));
app.use('/email', require ('./routes/emailRoutes.js'));

// Set the server to listen on port 3000
app.listen(port, () => console.log(`listening on port ${port}`))
// app.use(express.static(__dirname));
// app.use(express.static(path.join(__dirname, 'build')));
// app.get('/*', function (req, res) {
//   res.sendFile(path.join(__dirname, 'build', 'index.html'));
// });