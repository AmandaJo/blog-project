import React , { useState , useEffect } from 'react'
import Axios from 'axios'
import{url} from "../config.js" 

const Login = (props) => {
	const [ form , setValues ] = useState({
		email    : '',
		password : ''
	})
	const [ message , setMessage ] = useState('')
	useEffect( () => {
        props.isLoggedIn()
	},[])
	const handleChange = e => {
       setValues({...form,[e.target.name]:e.target.value})
	}
	const handleSubmit = async (e) => {
		e.preventDefault()
		try{
          const response = await Axios.post(`${url}/users/login`,{
          	email:form.email,
          	password:form.password
          })
          console.log('=== response ===>',response)
          setMessage(response.data.message)
          if( response.data.ok ){
              localStorage.setItem('token',JSON.stringify(response.data.token)) 
              setTimeout( ()=> props.isLoggedIn(),2000)     
          }
          
		}
        catch(error){
        	console.log(error)
        }
	}
	return <form onSubmit={handleSubmit}
	             onChange={handleChange}
	             className='form_container'>
	         <label>Email</label>    
		     <input name="email"/>
		     <label>Password</label>
		     <input name="password"/>
		     <button>login</button>
		     <div className='message'><h4>{message}</h4></div>
	       </form>
}

export default Login










