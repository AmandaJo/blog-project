import React from 'react'
import widgetStyle from './widgetStyle';

export default class UploadImages extends React.Component{

	uploadWidget = () => {
        window.cloudinary.openUploadWidget({ 
        	cloud_name: 'dlzv7cu6n', 
        	upload_preset: 'zzs0ajb2', 
			tags:['user'],
			stylesheet:widgetStyle
        },
            (error, result)=> {
				//debugger
                if(error){
						console.log(error)
                 }else{
                   this.props.uploadImage({
                   	               		   photo_url:result[0].secure_url, 
                                           public_id:result[0].public_id
										 })
				}	
            });
    }

	render(){
		return (
					<button className ="button_small"
                    	onClick={this.uploadWidget.bind(this)} > 
                      Upload Image
                    </button>
             
		)
	}
}

