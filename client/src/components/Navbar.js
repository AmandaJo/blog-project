import React from "react";
import { NavLink } from "react-router-dom";

const Navbar = (props) => {
  return (
    <div style={styles.navStyles}>
      <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/"}
      >
        Home
      </NavLink>

      <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/about"}
      >
        About
      </NavLink>
      
      <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/posts"}
      >
        Posts
      </NavLink>
      {
        props.loggedIn
        ?   <NavLink
              exact
              style={styles.default}
              activeStyle={styles.active}
              to={"/create_post"}>
            Create Post
            </NavLink>
        : null
      }

      <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/contact"}
      >
        Contact
      </NavLink>
      {
        !props.loggedIn
        ?  <NavLink
              exact
              style={styles.default}
              activeStyle={styles.active}
              to={"/login"}>
            Login
          </NavLink>
        : null
      }
  
      {
        !props.loggedIn
        ? <NavLink
              exact
              style={styles.default}
              activeStyle={styles.active}
              to={"/register"}>
            Register
          </NavLink>
        : null
      }
      {
        props.loggedIn
        ? <h5 onClick={()=>props.logout()}>Logout</h5>
        : null
      }
    </div>
  );
};

export default Navbar;
const styles = {
  navStyles: {
    display: "flex",
    height: 60,
    alignItems: "center",
    justifyContent: "space-around",
     background: "white",
     textTransform: "uppercase"
     
  },
  active: {
    color: "black"
  },
  default: {
    textDecoration: "none",
    color: "grey"
  }
};
