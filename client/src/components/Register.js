import React , { useState, useEffect } from 'react'
import Axios from 'axios' 
import { url } from "../config.js"

const Register = (props) => {
	const [ form , setValues ] = useState({})
	const [ message , setMessage ] = useState('')
    useEffect( () => {
        props.isLoggedIn()
	},[])
	const handleChange = e => {
       setValues({...form,[e.target.name]:e.target.value})
	}
	const handleSubmit = async (e) => {
		e.preventDefault()
		try{
			const response =  await Axios.post(`${url}/users/register`,{
	            email    : form.email,
	            username : form.username, 
			    password : form.password,
			    password2: form.password2
	        })
	        setMessage(response.data.message)
              if( response.data.ok ){ 
              setTimeout( ()=> props.history.push('/login'),2000)     
          }   
          
	        //console.log(response)
		}
		catch( error ){
			console.log(error)
		}

	}
	return <form onSubmit={handleSubmit}
	             onChange={handleChange}
	             className='form_container'>
	         <label>Email</label>
		     <input name="email"/>
		     <label>Username</label>
		     <input name="username"/>
		     <label>Password</label>
		     <input name="password"/>
		     <label>Repeat password</label>
		     <input name="password2"/>
		     <button>register</button>
		     <div className='message'><h4>{message}</h4></div>
	       </form>
}

export default Register