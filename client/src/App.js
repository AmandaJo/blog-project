import React , {useState,useEffect} from 'react';
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import axios from 'axios';
//========= containers ==========
import About   from "./containers/about.js";
import Home    from "./containers/home.js";
import Posts   from "./containers/posts.js";
import SinglePost    from "./containers/single_post.js";
import Contact from "./containers/contact.js";
import Create_post from "./containers/create_post.js";
import Login from './components/Login.js'
import Register from './components/Register.js'
import'./styles/App.css';
import { url } from "./config.js"
import Navbar from "./components/Navbar.js";
 
 function App() {
    const [loggedIn,setLoggedIn] = useState(false)
    const [posts, setPosts] = useState([]);
    const [show, setShow] = useState([]);
    const[error, setError] = useState('');
    const[categories, setCategories] = useState([]);
    const[email, setEmail]=useState();
    const[username, setUsername]=useState();

    useEffect( ()=> {
      isLoggedIn()
      getCategories()
      getPosts()
    },[])
    // useEffect( ()=> {
    //   console.log('=============>',categories)
    // },[categories])
//================  get all posts  =========================
    const getCategories = async () => {
      try{ 
        const response = await axios.get(`${url}/category/get_all`)
        //console.log('categories ==>', response.data)
        setCategories([...categories,...response.data])
      }catch(error){
        console.log(error)
      }
    }

    const getPosts = async () => {
       try{
          const response = await axios.get(`${url}/posts/get_all`)
          //console.log('posts ==>', response.data)
          setPosts(response.data)//original
          setShow(response.data)//changes
       }
       catch(error){
          console.log(error)
       }
     }

     const isLoggedIn = async () => {
     const token = JSON.parse(localStorage.getItem('token'))
       try{
          const response = await axios.post(`${url}/users/verify_token`,{token})
          return response.data.ok ? (setLoggedIn(true),setUsername(response.data.decoded.username)) : null
          //console.log('response ver token ==>', response.data)
       }
       catch(error){
          console.log(error)
       }
     }
     const logout = () => {
        //console.log('=====.  ======.  ======. ')
       localStorage.removeItem('token')
       setLoggedIn(false)
       setUsername('')
     }

      const handleCategoryChange = (e) => {
           if(e.target.value !== 'all'){
               const filtered = posts.filter( ele => ele.category === e.target.value)
               setShow(filtered)  
           }else{
              setShow(posts)
           }
           

      }
 

    return (
      <Router>
      <div>
          <header>
           <h1>INFO</h1>
           </header>
          <Navbar loggedIn={loggedIn} logout={logout}/>
          <Route exact path="/" component={Home} />
          <Route path="/about"  component={About} />
          <Route path="/posts"  render = {(props)=><Posts 
                                                      handleCategoryChange={handleCategoryChange}
                                                      categories={categories}
                                                      posts = {show}
                                                      {...props}/>
                                                         }/>
          <Route path="/contact" component={Contact} />

          <Route path="/login" render={props => loggedIn
                                                ? <Redirect to='/'/>
                                                : <Login {...props} isLoggedIn={isLoggedIn}/> } />
          <Route path='/register'   render={props => loggedIn
                                                   ? <Redirect to='/'/>
                                                   : <Register{...props} isLoggedIn={isLoggedIn}/> } />
          <Route exact path="/single_post/:post_id" render={props => <SinglePost {...props} username={username}/>} /> 
          <Route exact path="/create_post" render = {(props)=><Create_post
                                                              username={username}
                                                              categories = {categories} 
                                                               {...props}/>
                                                              }/> 

           <footer class="page-footer font-small unique-color-dark pt-4">
        

            <div class="footer-copyright text-center py-3">© 2018 Copyright:
    <a href="https://mdbootstrap.com/education/bootstrap/"> Info.com</a>
                                                   
  </div>
           </footer>
          </div>
      </Router>
   )
}











export default App;


