import React, {useState} from "react";
import axios from 'axios';
import {url} from "../config.js"


const Contact = () => {
const [form,setForm] = useState({})

   const handleSubmit = async (e) => {
		e.preventDefault()
		if(!form.subject)return alert('Please create subject')
		console.log(form)
		try{
			const response =  await axios.post(`${url}/email/send_contact_email/`,{
	            email :form.email,
			    firstname:form.firstname,
			    lastname:form.lastname,
			    subject:form.subject,
			    message:form.message

			  
	        })
	        console.log(response.data)
		}
	    catch(error){
	        console.log(error)
	    }
  }
 const handleChange = e => setForm({...form, [e.target.name]:e.target.value})
return (
    <div className="App">
    <p className = 'contactp'>Contact Us</p>
    <div>
    <form onSubmit={handleSubmit} action="/action_page.php">
    <label>First Name</label>
    <input onChange={handleChange} type="text" id="fname" name="firstname" placeholder="Your name.." />
    <label>Last Name</label>
    <input onChange={handleChange}type="text" id="lname" name="lastname" placeholder="Your last name.." />
    

    <label>Email</label>
    <input onChange={handleChange} type="email" id="email" name="email" placeholder="Your email" />

   <label>Subject</label>
   <input onChange={handleChange} type="text" id="subject" name="subject" placeholder="Your subject" />

    <label>Message</label>
    <textarea onChange={handleChange} id="message" name="message" placeholder="Write something.."></textarea>
    <input onChange={handleChange} type="submit" value="Submit" />
 
    </form>
    </div>
    </div>
  );
}




export default Contact;
const styles = {
  marginTop: "20%",
  textAlign: "center"
};