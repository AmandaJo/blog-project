import React from 'react';
import axios from 'axios';
import { NavLink } from "react-router-dom";



const About = () => {
	
  return <div className='container'>
  <h1 className = 'about_h1'>The Brilliant Creators Of INFO</h1>
 <div className='first-collumn'>
  <img className= 'first-collumn-image'src={require("../images/zebra.jpg")}  alt="" />
   <p style={styles}> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
 ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation 
ullamco laboris nisi ut aliquip ex ea commodo onsequat. Duis aute irure dolor in 
reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
 </p>
 </div>
 <div className='second-collumn'>
 <img className= 'second-collumn-image'src={require("../images/sloth.jpg")}  alt="" />
 <p className='info'style={styles}> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
 ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation 
ullamco laboris nisi ut aliquip ex ea commodo onsequat. Duis aute irure dolor in 
reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
 </p>
 </div>
 <div className='third-collumn'>
 <img className= 'third-collumn-image'src={require("../images/hippo.jpg")}  alt="" />
 <p style={styles}> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
 ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation 
ullamco laboris nisi ut aliquip ex ea commodo onsequat. Duis aute irure dolor in 
reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
 </p>
 </div>
 </div>
 
};
export default About;
const styles = {
  marginTop: "10%",
  textAlign: "center",
  //text:"black"
};