import React, {useState} from "react";
import axios from 'axios';
import { Button, Comment, Form, Header } from 'semantic-ui-react'
import { url }  "../config.js"



const Create_comment = (props) => {
  const [form,setForm] = useState({})


  

  const handleSubmit = async (e) => {
		e.preventDefault()
		if(!form.comment)return alert('Please create a comment')
		console.log(form)
		try{
			const response =  await axios.post(`${url}/comment/create/`,{
	           comment :form.comment,
			   createdAt:form.createdAt,
			   creator:form.username
			  
	        })
	        console.log('do you have my user',response.data)
		}
	    catch(error){
	        console.log(error)
	    }
  }

  const CommentExampleAvatar = () => {
  <Comment.Group>
    <Comment>
      <Comment.Avatar src='/images/avatar/small/elliot.jpg' />
    </Comment>
  </Comment.Group>
}


  const handleChange = e => setForm({...form, [e.target.name]:e.target.value})
  //const[categories,setCategories] = useState([...props.categories])
  return<div>
           {commentExampleAvatar}
         <form onSubmit={handleSubmit}
	           className='create_comment_container'>
		     <label>Comment</label>
		     <textarea onChange={handleChange} name="comment"></textarea>
		     </form> 
            </div>
      
 
};
export default Create_comment;
const styles = {
  marginTop: "10%",
  textAlign: "center",
  //text:"black"
};