import React , {useState,useEffect} from 'react';
import UploadImages from "../components/Uploadimages.js";


const Posts = (props) => {
	console.log('props.posts',props.posts)
	const renderPosts = props.posts.map((ele, idx)=>{
	      return(
			       <div className='posts' key = {idx}>
                   <div className = 'card'>
			       <h2 style = {styles}>{ele.posts}</h2>
			       <p style = {styles}>{ele.creator}</p>
			       <img className='post-images' src= {ele.images[0].photo_url} alt = 'post-images'/>
                    <h5 style = {styles}>{ele.description.substring(0,250)}...
			                <strong onClick={()=>props.history.push(`/single_post/${ele._id}`)}>Read more</strong>
			       </h5>
                    </div>

			       </div> 
	      )
	    })

return <div className= 'categories'> 
            <select onChange={e => props.handleCategoryChange(e)}>
                   <option value = {'all'}>see all posts</option>
                {
                  props.categories.map( (ele,i) => {
                  	return <option value = {ele._id}>{ele.category}</option>

                  })
                }
            </select>
           {renderPosts}
        </div>

}




export default Posts;
const styles = {
  marginTop: "5%",
  textAlign: "center"

}


