import React , {useState,useEffect} from 'react';
import axios from 'axios';
import UploadImages from "../components/Uploadimages.js";
import { Button, Comment, Form, Header } from 'semantic-ui-react'
import {url} from "../config.js"

const SinglePost = (props) => {
    const [post, setPost] = useState({});
    const[error, setError] = useState('');
    const[comment,setComment] = useState('')
    const[comments,setComments] = useState([])
    const[loaded,setLoaded] = useState(false)
    useEffect( ()=> {
      get_post()
    },[])

    useEffect( () => {
       console.log('comments ==>', comments)
    },[comments])

    useEffect( ()=> {
      setLoaded(true)
    },[post])
  //============   GET POST AND RELATED COMMENTS COMMENTS  =================
	const get_post = async () => {
         console.log('==== props ======>',props)
       try{
          const response  = await axios.get(`${url}/posts/get_one/${props.match.params.post_id}`)
          const response2 = await axios.get(`${url}/comment/get_by_id/${response.data._id}`)
            
          //console.log('response.data ==>', response.data)
          //console.log('post.description==========>', post.description)
          setPost(response.data) 
          setComments(response2.data)


       }
       catch(error){
          console.log(error)
       }
  }
  //============   CREATE A COMMENT  =================
  const handleChange = e => setComment(e.target.value)
//console.log(' PROPS1 =======>',props)
  const handleSubmit = async (e) => {
    e.preventDefault()
    if(!comment)return alert('Please create a comment')
    //console.log(form)
    try{
      console.log('=======>',post)
      const response =  await axios.post(`${url}/comment/create/`,{
              comment:comment,
              createdAt: new Date().toDateString(),
              posts_id: post._id,
              creator:props.username
          })
          console.log(response.data)
          setComments([...comments,response.data])
    }
      catch(error){
          console.log(error)
      }
  }


  //console.log('######',post)
  return loaded 
         ? <div className = 'single_post_p'>
          <h1 style = {styles}>{post.posts}</h1>
             <p>Created by: {post.creator}</p>
      {
        post.images
        ? <img src={post.images[0].photo_url} alt = 'post-images'/>
        : null
      }
     
      <p style = {styles}>{post.description}</p>
      {/*
        post.images
        ? <img src={post.images[1].photo_url} alt = 'post-images'/>
        : null
      */}

      {/*<form onSubmit={handleSubmit}>
        <input  type="text" 
                onChange={handleChange} 
                name="comment" 
                value={comment}
                placeholder="Your comment.." />

        <button>Comment</button>
         {
          comments.map( (ele,i) => {
            return <h4>{ele.comment}</h4>
          })
         }
         </form>*/}
  <Comment.Group>
    <Header as='h3' dividing>
      Comments
    </Header>

        {
          comments.map( (ele,i) => {
            //console.log('what is here',ele)
            return     <Comment>
                          <Comment.Avatar src='/images/avatar/small/joe.jpg' />
                          <Comment.Content>
                            <Comment.name as='a'>{ele.creator}</Comment.name>
                            <Comment.Metadata>
                              <div>{ele.createdAt}</div>
                            </Comment.Metadata>
                            <Comment.Text>{ele.comment}</Comment.Text>
                            <Comment.Actions>
                            </Comment.Actions>
                          </Comment.Content>
                        </Comment>
          })
        }


    <Form reply onSubmit={handleSubmit}>
      <Form.TextArea onChange={handleChange}/>
      <Button content='Add Reply' labelPosition='left' icon='edit' primary />
    </Form>
  </Comment.Group>
      
      <div></div>
    </div>
    : null


}

     //console.log(props.match.params)
  

export default SinglePost; 
const styles = {
  marginTop: "5%",
  textAlign: "center"


}


