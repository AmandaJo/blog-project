import React, { useState } from 'react';
import axios from 'axios';
import UploadImages from "../components/Uploadimages.js";
import widgetStyle from '../components/widgetStyle';
import {url} from "../config.js"


const Create_post = (props) => {
  const [form,setForm] = useState({})
  const [images,setImages] = useState([])
  const [message,setMessage] = useState('')
  

  const handleSubmit = async (e) => {
		e.preventDefault()
		if(images.length !== 2)return alert('Please upload two images !!')

		//console.log('=========>',form)
		try{
			const response =  await axios.post(`${url}/posts/create/`,{
  	        title :form.title,
  			    description:form.description,
  			    creator:props.username,
  			    images:images,
  			    category:form.category
	        })
          return response.data.ok
          ? (setMessage(response.data.message), setTimeout( ()=> {props.history.push(`/single_post/${response.data.response._id}`)},2000))
          : setMessage(response.data.message)
          //setTimeout( ()=> {setMessage('')},1500)
	        
		}
	    catch(error){
	        console.log(error)
	    }
  }

  const handleChange = e => setForm({...form, [e.target.name]:e.target.value})
  const uploadImage = image => setImages([...images, image])
  const handleCategoryChange = e => setForm({...form, category:e.target.value})
  //const[categories,setCategories] = useState([...props.categories])
  return<div>
         <h1>{message}</h1>
         <form onSubmit={handleSubmit}
	           className='create_post_container'>
	         <label>title</label>
		     <input onChange={handleChange} name="title"/>
		     <label>Content</label>
		     <textarea onChange={handleChange} name="description"></textarea>
            <label>Select Category:
            <select onChange={handleCategoryChange}>
                {
                  props.categories.map( (ele,i) => {
                  	return <option value = {ele._id}>{ele.category}</option>

                  })
                }
            </select>
            </label>
         
   		    <button >Submit post</button>

	       </form>
         {
          images.length < 3
          ? <UploadImages uploadImage={uploadImage}/>
          : null
         }
           
           {
            images.map((ele,i) =><img key={i} src={ele.photo_url}/>)
                 
          }
           
      </div>
 
};
export default Create_post;
const styles = {
  marginTop: "10%",
  textAlign: "center",
  //text:"black"
};